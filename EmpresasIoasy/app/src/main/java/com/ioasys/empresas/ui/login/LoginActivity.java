package com.ioasys.empresas.ui.login;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.google.android.material.textfield.TextInputLayout;
import com.ioasys.empresas.R;
import com.ioasys.empresas.api.RetrofitInstance;
import com.ioasys.empresas.model.Usuario;
import com.ioasys.empresas.model.api.UserForAuth;
import com.ioasys.empresas.ui.empresa.EmpresasActivity;
import com.ioasys.empresas.ui.viewmodel.LoginViewModel;
import com.ioasys.empresas.ui.viewmodel.ViewModelFactory;
import com.ioasys.empresas.util.PreferenceUtil;

import org.jetbrains.annotations.NotNull;

public class LoginActivity extends AppCompatActivity {

    private LoginViewModel viewModel;
    private TextView tvErro;
    private TextInputLayout etEmail;
    private TextInputLayout etSenha;
    private FrameLayout flProgress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initViewModel();
        initView();
    }

    private void initViewModel() {
        hasUser();
        viewModel = new ViewModelFactory().create(LoginViewModel.class);
    }

    private void hasUser() {
        Usuario usuario = PreferenceUtil.getUsuario();
        if(usuario != null && usuario.getAccessToken() != null && usuario.getClient() != null && usuario.getUid() != null){
            configureRetrofit(usuario);
            startActivity(EmpresasActivity.getStartIntent(this));
            finish();
        }
    }

    private void initView() {

        etEmail = findViewById(R.id.etEmailLayout);
        etSenha = findViewById(R.id.etPasswordLayout);
        flProgress = findViewById(R.id.progressBarHolder);
        tvErro = findViewById(R.id.tvErroMensage);
        Button btEntrar = findViewById(R.id.btnEntrar);

        btEntrar.setOnClickListener(v -> {
            showProgress(true);
            UserForAuth userForAuth = createUserForAuth();
            viewModel.getUsuario(userForAuth);
            viewModel.getUserAuthentic().observe(this, wrapper -> {
                showProgress(false);
                if(wrapper.getData() != null && wrapper.getData().getAccessToken() != null){
                    Usuario data = wrapper.getData();
                    enableMensage(false,"");
                    configureRetrofit(data);
                    PreferenceUtil.persistUsuario(data);
                    startActivity(EmpresasActivity.getStartIntent(this));
                } else if (wrapper.getMessage() != null && !wrapper.getMessage().isEmpty()){
                    // Trataria utilizando codigo de erro caso a API retornasse
                    if(wrapper.getMessage().contains("Invalid login")){
                        showErrorMessage(1);
                    }
                } else {
                    showErrorMessage(-1);
                }
            });
        });

    }

    private void configureRetrofit(Usuario data) {
        RetrofitInstance retrofitInstance = RetrofitInstance.getRetrofitClient();
        retrofitInstance.setHeaders(data.getAccessToken(),data.getClient(),data.getUid());
    }

    private void showErrorMessage(int i) {
        String messageErro = "";
        switch (i){
            case -1:
                messageErro = getString(R.string.menssage_error_inesperado);
            case 1:
                messageErro = getString(R.string.menssage_error_credenciais_invalidas);
        }
        enableMensage(true,messageErro);

    }

    @NotNull
    private UserForAuth createUserForAuth() {
        String email = etEmail.getEditText().getText().toString();
        String senha = etSenha.getEditText().getText().toString();
        return new UserForAuth(email,senha);
    }

    private void enableMensage(boolean enable, String mensage){
        tvErro.setVisibility(enable ? View.VISIBLE : View.GONE);
        tvErro.setText(mensage);

    }

    private void showProgress(boolean enable){
        flProgress.setVisibility(enable ? View.VISIBLE : View.GONE);

    }
}