package com.ioasys.empresas.ui.empresa;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.ioasys.empresas.R;
import com.ioasys.empresas.api.ApiService;
import com.ioasys.empresas.model.Empresa;

import java.util.ArrayList;
import java.util.List;

public class EmpresaAdapter extends RecyclerView.Adapter<EmpresaAdapter.EmpresaViewHolder> {

    public static final String BASE_IMAGE = "https://empresas.ioasys.com.br";
    private List<Empresa> empresas = new ArrayList<>();
    private Context context;

    public EmpresaAdapter(Context context){
        this.context = context;
    }

    public void setEmpresas(List<Empresa> empresas) {
        this.empresas.clear();
        this.empresas.addAll(empresas);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public EmpresaViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_cardview_empresas, parent, false);
        return new EmpresaViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull EmpresaViewHolder holder, int position) {
        Empresa empresa = empresas.get(position);
        String imagem = BASE_IMAGE + empresa.getImagem();
        Glide.with(context).load(imagem).apply(new RequestOptions().override(105, 80)).into(holder.ivEmpresa);
        holder.tvNomeEmpresa.setText(empresa.getNome());
        holder.tvTipoEmpresa.setText(empresa.getTipoEmpresa().toString());
        holder.tvPaisEmpresa.setText(empresa.getPais());
        holder.itemView.setOnClickListener(v -> {
            context.startActivity(DetalheEmpresaActivity.getStartIntent(context, empresa.getId()));
        });

    }

    @Override
    public int getItemCount() {
        return empresas.size();
    }

    public class EmpresaViewHolder extends RecyclerView.ViewHolder{
        private ImageView ivEmpresa;
        private TextView tvNomeEmpresa;
        private TextView tvTipoEmpresa;
        private TextView tvPaisEmpresa;

        public EmpresaViewHolder(@NonNull View itemView) {
            super(itemView);
            ivEmpresa = itemView.findViewById(R.id.ivEmpresa);
            tvNomeEmpresa = itemView.findViewById(R.id.tvNomeEmpresa);
            tvTipoEmpresa = itemView.findViewById(R.id.tvTipoEmpresa);
            tvPaisEmpresa = itemView.findViewById(R.id.tvPais);
        }
    }
}
