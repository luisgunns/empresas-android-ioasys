package com.ioasys.empresas.model.api;

public class UserForAuth {

    private String email;
    private String password;

    public UserForAuth(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public UserForAuth(){

    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


}
