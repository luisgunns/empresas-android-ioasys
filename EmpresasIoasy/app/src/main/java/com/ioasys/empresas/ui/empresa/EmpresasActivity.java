package com.ioasys.empresas.ui.empresa;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import com.ioasys.empresas.R;
import com.ioasys.empresas.model.Empresa;
import com.ioasys.empresas.model.api.Empresas;
import com.ioasys.empresas.ui.viewmodel.EmpresaViewModel;
import com.ioasys.empresas.ui.viewmodel.ViewModelFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class EmpresasActivity extends AppCompatActivity {

    private EmpresaViewModel viewModel;
    private List<Empresa> empresas;
    private RecyclerView viewById;
    private EmpresaAdapter adapter;
    private ViewFlipper vfEmpresa;

    public static Intent getStartIntent(Context ctx) {
        return new Intent(ctx, EmpresasActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_empresas);
        configureActionBar();
        initViewModel();
        initView();
    }

    private void configureActionBar() {
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setLogo(R.drawable.logo_nav);
        getSupportActionBar().setDisplayUseLogoEnabled(true);
        getSupportActionBar().setTitle("");
    }


    private void initView() {
        vfEmpresa = findViewById(R.id.vfContainer);
    }

    private void initViewModel() {
        viewModel = new ViewModelFactory().create(EmpresaViewModel.class);

        viewById = findViewById(R.id.rvEmpresas);
        viewById.setLayoutManager(new LinearLayoutManager(this));
        viewById.setHasFixedSize(true);
        adapter = new EmpresaAdapter(this);
        viewById.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_empresas, menu);



        MenuItem item = menu.findItem(R.id.action_search);
        SearchView actionView = (SearchView) item.getActionView();


        actionView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                viewModel.buscarEmpresa(query);
                viewModel.getEmpresa().observe(EmpresasActivity.this, wrapper -> {
                    Empresas empresasList = wrapper.getData();
                    if(empresasList != null && empresasList.getEmpresas() != null && !empresasList.getEmpresas().isEmpty()){
                        empresas = empresasList.getEmpresas();
                        showPlaceHolder(false);
                        adapter.setEmpresas(empresas);
                    } else if (wrapper.getMessage() != null && wrapper.getMessage().equals(Empresas.EMPTY_LIST)){
                        showPlaceHolder(true);
                    } else {
                        Toast.makeText(EmpresasActivity.this, getResources().getString(R.string.menssage_error_inesperado), Toast.LENGTH_LONG).show();
                    }
                });
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return true;
            }
        });
        return true;
    }

    private void showPlaceHolder(boolean enable) {
        vfEmpresa.setDisplayedChild(enable ? 1 : 0);
    }

}