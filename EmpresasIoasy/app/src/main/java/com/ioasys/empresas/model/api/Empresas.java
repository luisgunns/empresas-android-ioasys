package com.ioasys.empresas.model.api;

import com.google.gson.annotations.SerializedName;
import com.ioasys.empresas.model.Empresa;

import java.util.List;

public class Empresas {
    public static final String EMPTY_LIST = "empty";

    @SerializedName("enterprises")
    private List<Empresa> empresas;

    @SerializedName("enterprise")
    private Empresa empresasDetalhe;

    public List<Empresa> getEmpresas() {
        return empresas;
    }

    public void setEmpresas(List<Empresa> empresas) {
        this.empresas = empresas;
    }

    public Empresa getEmpresasDetalhe() {
        return empresasDetalhe;
    }

    public void setEmpresasDetalhe(Empresa empresasDetalhe) {
        this.empresasDetalhe = empresasDetalhe;
    }
}
