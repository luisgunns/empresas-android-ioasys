package com.ioasys.empresas.util;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.ioasys.empresas.EmpresasApplication;
import com.ioasys.empresas.model.Usuario;

public class PreferenceUtil {

    private static Context context;

    static {
        context = EmpresasApplication.getContext();
    }

    public static void persistUsuario(Usuario usuario) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(Usuario.usuarioTag, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(Usuario.usuarioTag, new Gson().toJson(usuario));
        editor.apply();
    }

    public static Usuario getUsuario() {
        SharedPreferences preferences = context.getSharedPreferences(Usuario.usuarioTag, Context.MODE_PRIVATE);
        return new Gson().fromJson(preferences.getString(Usuario.usuarioTag, null), Usuario.class);
    }

    public void clearUsuario() {
        SharedPreferences preferences = context.getSharedPreferences(Usuario.usuarioTag, Context.MODE_PRIVATE);
        preferences.edit().clear().apply();
    }
}
