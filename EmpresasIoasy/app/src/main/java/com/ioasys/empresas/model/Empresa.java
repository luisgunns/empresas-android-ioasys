package com.ioasys.empresas.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class Empresa implements Parcelable {

    @SerializedName("enterprise_name")
    private String nome;
    @SerializedName("enterprise_type")
    private TipoEmpresa tipoEmpresa;
    @SerializedName("country")
    private String pais;
    @SerializedName("photo")
    private String imagem;
    @SerializedName("description")
    private String descricao;
    @SerializedName("id")
    private int id;

    public String getImagem() {
        return imagem;
    }

    public void setImagem(String imagem) {
        this.imagem = imagem;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public TipoEmpresa getTipoEmpresa() {
        return tipoEmpresa;
    }

    public void setTipoEmpresa(TipoEmpresa tipoEmpresa) {
        this.tipoEmpresa = tipoEmpresa;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.nome);
        dest.writeParcelable(this.tipoEmpresa, flags);
        dest.writeString(this.pais);
        dest.writeString(this.imagem);
        dest.writeString(this.descricao);
        dest.writeInt(this.id);
    }

    public void readFromParcel(Parcel source) {
        this.nome = source.readString();
        this.tipoEmpresa = source.readParcelable(TipoEmpresa.class.getClassLoader());
        this.pais = source.readString();
        this.imagem = source.readString();
        this.descricao = source.readString();
        this.id = source.readInt();
    }

    public Empresa() {
    }

    protected Empresa(Parcel in) {
        this.nome = in.readString();
        this.tipoEmpresa = in.readParcelable(TipoEmpresa.class.getClassLoader());
        this.pais = in.readString();
        this.imagem = in.readString();
        this.descricao = in.readString();
        this.id = in.readInt();
    }

    public static final Parcelable.Creator<Empresa> CREATOR = new Parcelable.Creator<Empresa>() {
        @Override
        public Empresa createFromParcel(Parcel source) {
            return new Empresa(source);
        }

        @Override
        public Empresa[] newArray(int size) {
            return new Empresa[size];
        }
    };
}
