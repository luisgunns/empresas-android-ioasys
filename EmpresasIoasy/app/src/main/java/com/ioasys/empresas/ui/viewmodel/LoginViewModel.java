package com.ioasys.empresas.ui.viewmodel;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.ioasys.empresas.data.Repository;
import com.ioasys.empresas.model.api.UserForAuth;
import com.ioasys.empresas.model.Usuario;
import com.ioasys.empresas.model.livedata.DataWrapper;

public class LoginViewModel extends ViewModel {
    private Repository repository;
    private MutableLiveData<DataWrapper<Usuario, String>> userAuthentic;

    public LoginViewModel() {
        this.repository = Repository.getInstance();
    }

    public void getUsuario(UserForAuth userAuth){
        userAuthentic = repository.getUserAuthentic(userAuth);
    }

    public MutableLiveData<DataWrapper<Usuario, String>> getUserAuthentic() {
        return userAuthentic;
    }
}
