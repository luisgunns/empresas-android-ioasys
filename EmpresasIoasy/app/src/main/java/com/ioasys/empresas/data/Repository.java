package com.ioasys.empresas.data;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.ioasys.empresas.api.ApiService;
import com.ioasys.empresas.api.RetrofitInstance;
import com.ioasys.empresas.model.Empresa;
import com.ioasys.empresas.model.api.Empresas;
import com.ioasys.empresas.model.api.UserForAuth;
import com.ioasys.empresas.model.Usuario;
import com.ioasys.empresas.model.livedata.DataWrapper;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Headers;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Repository {
    private static final String TAG = "Repos";
    private static Repository instance;

    public static Repository getInstance(){
        if(instance == null){
            instance = new Repository();
        }
        return instance;
    }

    public MutableLiveData<DataWrapper<Usuario,String>> getUserAuthentic(UserForAuth user){
        MutableLiveData<DataWrapper<Usuario,String>> liveData = new MutableLiveData<>();
        DataWrapper<Usuario,String> data = new DataWrapper<>();
        ApiService apiService = RetrofitInstance.getRetrofitClient().getInstance().create(ApiService.class);
        Call<Usuario> service = apiService.getUsuario(user);
        service.enqueue(new Callback<Usuario>() {
            @Override
            public void onResponse(Call<Usuario> call, Response<Usuario> response) {
                Headers headers = response.headers();
                if(response.isSuccessful()){
                    Usuario usuario = makeUser(headers);
                    data.setData(usuario);
                    liveData.postValue(data);
                } else {
                    String error = null;
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        Log.e("ReposLogin", "onResponse: " + jObjError);
                        error = ((JSONArray) jObjError.get("errors")).toString();
                    } catch (JSONException e) {
                        e.printStackTrace();
                        error = e.getMessage();
                    } catch (IOException e) {
                        e.printStackTrace();
                        error = e.getMessage();
                    } finally {
                        data.setMessage(error);
                        liveData.postValue(data);
                    }
                }
            }

            @Override
            public void onFailure(Call<Usuario> call, Throwable t) {
                String message = t.getMessage();
                t.printStackTrace();
                data.setMessage(message);
                liveData.postValue(data);
            }
        });
        return liveData;
    }

    @NotNull
    private Usuario makeUser(Headers headers) {
        String client = headers.get("client");
        String accessToken = headers.get("access-token");
        String uid = headers.get("uid");
        return new Usuario(accessToken,client,uid);
    }


    public MutableLiveData<DataWrapper<Empresas, String>> getEmpresa(String nomeEmpresa){
        MutableLiveData<DataWrapper<Empresas,String>> liveData = new MutableLiveData<>();
        DataWrapper<Empresas,String> data = new DataWrapper<>();
        ApiService apiService = RetrofitInstance.getRetrofitClient().getInstance().create(ApiService.class);
        Call<Empresas> service = apiService.getEmpresa(nomeEmpresa);
        service.enqueue(new Callback<Empresas>() {
            @Override
            public void onResponse(Call<Empresas> call, Response<Empresas> response) {
                if(response.isSuccessful()){
                    Empresas empresa = response.body();
                    if(!empresa.getEmpresas().isEmpty()){
                        data.setData(empresa);
                    } else {
                        data.setMessage(Empresas.EMPTY_LIST);
                    }
                } else {
                    data.setMessage(response.message());
                }
                liveData.postValue(data);
            }

            @Override
            public void onFailure(Call<Empresas> call, Throwable t) {
                data.setMessage(t.getMessage());
                Log.e(TAG, t.getMessage() );
                liveData.postValue(data);
            }
        });

        return liveData;
    }

    public MutableLiveData<DataWrapper<Empresas, String>> getDetalheEmpresa(int id){
        MutableLiveData<DataWrapper<Empresas,String>> liveData = new MutableLiveData<>();
        DataWrapper<Empresas,String> data = new DataWrapper<>();
        ApiService apiService = RetrofitInstance.getRetrofitClient().getInstance().create(ApiService.class);
        Call<Empresas> service = apiService.getDetalheEmpresa(id);
        service.enqueue(new Callback<Empresas>() {
            @Override
            public void onResponse(Call<Empresas> call, Response<Empresas> response) {
                if(response.isSuccessful()){
                    Empresas empresa = response.body();
                    if(empresa != null  && empresa.getEmpresasDetalhe() != null){
                        data.setData(empresa);
                    } else {
                        data.setMessage(response.message());
                    }
                } else {
                    data.setMessage(response.message());
                }

                liveData.postValue(data);
            }

            @Override
            public void onFailure(Call<Empresas> call, Throwable t) {
                data.setMessage(t.getMessage());
                Log.e(TAG, t.getMessage() );
                liveData.postValue(data);
            }
        });
        return liveData;
    }
}
