package com.ioasys.empresas.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class TipoEmpresa implements Parcelable {

    @SerializedName("id")
    private int id;
    @SerializedName("enterprise_type_name")
    private String empresaTipo;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmpresaTipo() {
        return empresaTipo;
    }

    public void setEmpresaTipo(String empresaTipo) {
        this.empresaTipo = empresaTipo;
    }

    @Override
    public String toString() {
        return empresaTipo;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.empresaTipo);
    }

    public void readFromParcel(Parcel source) {
        this.id = source.readInt();
        this.empresaTipo = source.readString();
    }

    public TipoEmpresa() {
    }

    protected TipoEmpresa(Parcel in) {
        this.id = in.readInt();
        this.empresaTipo = in.readString();
    }

    public static final Parcelable.Creator<TipoEmpresa> CREATOR = new Parcelable.Creator<TipoEmpresa>() {
        @Override
        public TipoEmpresa createFromParcel(Parcel source) {
            return new TipoEmpresa(source);
        }

        @Override
        public TipoEmpresa[] newArray(int size) {
            return new TipoEmpresa[size];
        }
    };
}
