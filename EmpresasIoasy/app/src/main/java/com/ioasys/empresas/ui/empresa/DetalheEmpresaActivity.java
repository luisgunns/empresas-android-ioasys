package com.ioasys.empresas.ui.empresa;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.ioasys.empresas.R;
import com.ioasys.empresas.model.Empresa;
import com.ioasys.empresas.ui.viewmodel.EmpresaViewModel;
import com.ioasys.empresas.ui.viewmodel.ViewModelFactory;

import org.jetbrains.annotations.NotNull;

public class DetalheEmpresaActivity extends AppCompatActivity {

    private static final String EXTRA_ID = "ID_EMPRESA";
    private Empresa data;
    private EmpresaViewModel viewModel;
    private int id;
    private ImageView ivEmpresa;
    private TextView tvDescEmpresa;

    public static Intent getStartIntent(Context ctx, int id) {
        return new Intent(ctx, DetalheEmpresaActivity.class)
                .putExtra(EXTRA_ID, id);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalhe_empresa);
        initView();
        initViewModel();
    }

    private void initView() {
        ivEmpresa = findViewById(R.id.ivEmpresaDetalhe);
        tvDescEmpresa = findViewById(R.id.tvDescEmpresaDetalhes);
    }

    @NotNull
    private void initViewModel() {
        id = getIntent().getExtras().getInt(EXTRA_ID);
        viewModel = new ViewModelFactory().create(EmpresaViewModel.class);
        viewModel.buscarDetalhesEmpresa(id);
        viewModel.getDetalheEmpresa().observe(this, wrapper -> {
            if(wrapper.getData() !=  null ){
                data = wrapper.getData().getEmpresasDetalhe();
                initWithData(data);
            } else {
                Toast.makeText(this, getResources().getString(R.string.menssage_error_inesperado), Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void initWithData(Empresa empresa) {
        String imagem = EmpresaAdapter.BASE_IMAGE + empresa.getImagem();
        Glide.with(this).load(imagem).into(ivEmpresa);
        tvDescEmpresa.setText(empresa.getDescricao());
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}