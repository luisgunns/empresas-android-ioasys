package com.ioasys.empresas.api;

import com.ioasys.empresas.model.Empresa;
import com.ioasys.empresas.model.api.Empresas;
import com.ioasys.empresas.model.api.UserForAuth;
import com.ioasys.empresas.model.Usuario;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiService {

    public static final String BASE_URL = "https://empresas.ioasys.com.br/api/v1/";

    @POST("users/auth/sign_in")
    Call<Usuario> getUsuario(@Body UserForAuth user);


    @GET("enterprises")
    Call<Empresas> getEmpresa(@Query("name") String nome);

    @GET("enterprises/{id}")
    Call<Empresas> getDetalheEmpresa(@Path("id") int id);
}
