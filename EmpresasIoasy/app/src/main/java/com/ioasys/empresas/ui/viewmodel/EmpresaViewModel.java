package com.ioasys.empresas.ui.viewmodel;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.ioasys.empresas.data.Repository;
import com.ioasys.empresas.model.Empresa;
import com.ioasys.empresas.model.api.Empresas;
import com.ioasys.empresas.model.livedata.DataWrapper;

public class EmpresaViewModel extends ViewModel {
    private Repository repository;
    private MutableLiveData<DataWrapper<Empresas, String>> empresa;
    private MutableLiveData<DataWrapper<Empresas, String>> empresaDetalhe;

    public EmpresaViewModel() {
        this.repository = Repository.getInstance();
    }

    public void buscarEmpresa(String nomeEmpresa){
        empresa = repository.getEmpresa(nomeEmpresa);
    }

    public MutableLiveData<DataWrapper<Empresas, String>> getEmpresa() {
        return empresa;
    }

    public void buscarDetalhesEmpresa(int id){
        empresaDetalhe = repository.getDetalheEmpresa(id);
    }

    public MutableLiveData<DataWrapper<Empresas, String>> getDetalheEmpresa() {
        return empresaDetalhe;
    }
}
