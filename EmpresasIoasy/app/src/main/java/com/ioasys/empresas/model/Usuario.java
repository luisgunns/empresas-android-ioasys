package com.ioasys.empresas.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Usuario implements Parcelable {
    public static final String usuarioTag = "USUARIO";

    private String accessToken;
    private String client;
    private String uid;

    public Usuario(String accessToken, String client, String uid) {
        this.accessToken = accessToken;
        this.client = client;
        this.uid = uid;
    }

    public Usuario(){

    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.accessToken);
        dest.writeString(this.client);
        dest.writeString(this.uid);
    }

    public void readFromParcel(Parcel source) {
        this.accessToken = source.readString();
        this.client = source.readString();
        this.uid = source.readString();
    }

    protected Usuario(Parcel in) {
        this.accessToken = in.readString();
        this.client = in.readString();
        this.uid = in.readString();
    }

    public static final Parcelable.Creator<Usuario> CREATOR = new Parcelable.Creator<Usuario>() {
        @Override
        public Usuario createFromParcel(Parcel source) {
            return new Usuario(source);
        }

        @Override
        public Usuario[] newArray(int size) {
            return new Usuario[size];
        }
    };
}
