package com.ioasys.empresas.api;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitInstance {
    private Retrofit instance;
    private static RetrofitInstance retrofit;

    public static RetrofitInstance getRetrofitClient() {
        if (retrofit == null){
            retrofit = new RetrofitInstance();
            retrofit.setInstance(new Retrofit.Builder()
                    .baseUrl(ApiService.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build());
        }
        return retrofit;
    }

    public Retrofit getInstance() {
        return instance;
    }

    private void setInstance(Retrofit instance) {
        this.instance = instance;
    }

    public void setHeaders(String accessToken, String client, String uid) {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.addInterceptor(chain -> {
            Request original = chain.request();
            Request request = original.newBuilder()
                    .header("access-token", accessToken)
                    .header("client", client)
                    .header("uid", uid)
                    .build();
            return chain.proceed(request);
        });
        instance = instance.newBuilder().client(builder.build()).build();
    }
}
